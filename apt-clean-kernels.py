#!/usr/bin/env python3

import sys
import os
from collections import defaultdict
import logging
import optparse
import apt_pkg

log = logging.getLogger('apt-clean-kernels')

apt_pkg.init_system()
apt_pkg.init_config()

KERNEL_PREFIX = 'linux-image'
METAPKG_SIZE_MAX = 409600
KEEP_LINKS = ['/vmlinuz', '/vmlinuz.old']

####

# From apt_pkg documentation: "This value may be used in combination with has_versions to check whether a package is
# virtual; that is, it has no versions and is provided at least once"
if hasattr(apt_pkg.Package, 'has_provides') and hasattr(apt_pkg.Package, 'has_versions'):
    def is_virtual(pkg):
        return pkg.has_provides and not pkg.has_versions
else:
    # Compatibility with older python-apt versions
    def is_virtual(pkg):
        return bool(pkg.provides_list) and not pkg.version_list


if hasattr(apt_pkg.Package, 'get_fullname'):
    def getname(pkg):
        return pkg.get_fullname(True)
else:
    def getname(pkg):
        return pkg.name


def is_metapkg(pkg):
    """Returns True if given package is a metapackage.

    http://askubuntu.com/questions/193361/how-to-determine-if-a-package-is-a-meta-package-from-the-command-line
    "There is no formal definition of a metapackage. The informal definition is that a metapackage is one that is only
    intended to be installed for its dependencies and contains no useful file of its own."

    Since kernel images are quite large, we can probably get away with comparing by size.
    XXX any better approaches?
    """

    return pkg.version_list and pkg.version_list[0].installed_size <= METAPKG_SIZE_MAX


def get_meta_deps(pkg):
    """Walk through installed packages dependant on "pkg" and keep the ones that are metapackages"""
    return (dep.parent_pkg for dep in pkg.rev_depends_list
            if dep.dep_type_enum == apt_pkg.Dependency.TYPE_DEPENDS
            and dep.parent_pkg.selected_state == apt_pkg.SELSTATE_INSTALL
            and is_metapkg(dep.parent_pkg))

####


class Abort(Exception):
    pass


class Cleaner(object):
    def __init__(self, opts):
        self.opts = opts

        if opts.quiet:
            self.cache = apt_pkg.Cache(progress=None)
        else:
            self.cache = apt_pkg.Cache()

        # List of kernel packages to keep
        self.keep = []
        # All installed kernel images (non-metapackages)
        self.pkgs = []
        # Mapping from metapackages to kernels that provide the metapackage
        self.candidates = defaultdict(list)

    def find_kernels(self):
        # Find kernel virtual packages (e.g. linux-image, linux-image-2.6, linux-image-3.0)
        # We walk through all their dependencies to find actual kernel image files.
        virtuals = [p for p in self.cache.packages if p.name.startswith(KERNEL_PREFIX) and is_virtual(p)]

        if not virtuals:
            raise Abort("Could not find any virtual packages starting with %r" % KERNEL_PREFIX)

        # Find all installed packages that provide these virtuals
        for virt in virtuals:
            kernels = [provide[2].parent_pkg for provide in virt.provides_list]
            inst = [p for p in kernels if p.selected_state == apt_pkg.SELSTATE_INSTALL]
            if all:
                log.debug("Virtual package: %s (%d installed, %d total)", getname(virt), len(inst), len(kernels))
                self.pkgs.extend(inst)

                for pkg in inst:
                    for meta_pkg in get_meta_deps(pkg):
                        log.debug("%s -> %s", getname(pkg), getname(meta_pkg))
                        cand = self.candidates[getname(meta_pkg)]
                        cand.append(pkg)

        if not self.pkgs:
            raise Abort("Could not find installed kernel packages")

    def keep_metapkgs(self):
        # Find installed kernel metapackages (e.g. linux-image-generic, linux-image-lowlatency-pae)
        # We always keep the latest installed kernel in each metapackage
        metapkgs = [p for p in self.cache.packages
                    if p.name.startswith(KERNEL_PREFIX)
                    and p.selected_state == apt_pkg.SELSTATE_INSTALL
                    and is_metapkg(p)]

        if not metapkgs:
            log.error("Could not find installed metapackages with prefix %r", KERNEL_PREFIX)
            raise Abort("Please install metapackage such as 'linux-image-generic'")

        log.debug("Required metapackages: %s", ', '.join(getname(p) for p in metapkgs))

        log.info("Keeping:")

        for meta in metapkgs:
            cands = self.candidates[getname(meta)]
            if not cands:
                # XXX Debian warns: No candidates found for installed metapackage 'linux-image-2.6-amd64'
                # This is actually OK, should we walk dependencies recursively?
                log.debug("No candidates found for installed metapackage %r" % getname(meta))
                continue

            # Sometimes we find multiple kernel versions required by the same metapackage
            # Find greatest version to keep and get its package
            keep = max(p.current_ver for p in cands).parent_pkg

            log.info("* Installed %s: %s", getname(meta), getname(keep))
            self.keep.append(keep)

    def keep_symlinked(self):
        """Add KEEP_LINKS symlinked kernels to self.keep"""

        for linkname in KEEP_LINKS:
            pkg = get_symlink_pkg(self.cache, linkname)
            if pkg:
                log.info("* Symlink %s: %s", linkname, getname(pkg))
                self.keep.append(pkg)

    def keep_running(self):
        """Add running kernel package to self.keep"""

        running_pkg = get_running_kernel_pkg(self.cache)
        if running_pkg:
            log.info("* Running kernel: %s", getname(running_pkg))
            self.keep.append(running_pkg)

    def get_remove_kernels(self):
        keep_ids = set(pkg.id for pkg in self.keep)
        remove = []

        for pkg in self.pkgs:
            if pkg.id not in keep_ids:
                assert not pkg.essential, "Package %s is essential!" % getname(pkg)
                remove.append(pkg)

        return remove

    def prompt_remove(self):
        # Get a sorted list of unique package names. FIXME why does get_remove_kernels() return duplicates?
        remove_pkgs = self.get_remove_kernels()
        remove = sorted(set(getname(pkg) for pkg in remove_pkgs))

        if not remove:
            log.warning("Nothing to remove.")
            return

        log.info("Removing:")
        for name in remove:
            log.info("* %s", name)

        self.do_remove(remove)

    def do_remove(self, remove):
        cmd = []
        if os.getuid() != 0:
            cmd.append('sudo')

        cmd.extend(['apt-get', 'remove', '--'])
        cmd.extend(remove)

        log.debug("Running %s", ' '.join(cmd))
        log.info("")
        os.execvp(cmd[0], cmd)

    def run(self):
        self.find_kernels()

        self.keep_metapkgs()
        self.keep_running()
        self.keep_symlinked()

        self.prompt_remove()

####


def get_running_kernel_pkg(cache):
    ver = os.uname()[2]
    # TODO: map uname()[4] (architecture) to Debain arch? linux-image-foo:arch
    pkgname = '%s-%s' % (KERNEL_PREFIX, ver)
    if pkgname not in cache:
        raise Abort("Cannot find running kernel package %s. Aborting for everyone's safety" % pkgname)

    running_pkg = cache[pkgname]
    if running_pkg.selected_state != apt_pkg.SELSTATE_INSTALL:
        log.warning("Warning: Running kernel package %s not installed (state=%d)",
                    getname(running_pkg), running_pkg.selected_state)
    return running_pkg


def get_symlink_pkg(cache, filename):
    # XXX we could extract uname from the kernel image itself, or we could
    # query the package database to see which package the file belongs to.
    #
    # But simply parsing the file name works fine and seems the easiest.

    try:
        try_path = "/boot" + filename
        if not os.path.exists(try_path):
            try_path = filename
        name = os.path.basename(os.path.realpath(try_path))
    except (OSError, IOError) as err:
        log.warning("Warning: Cannot determine %s: %s", filename, err)
        return

    if '-' not in name:
        log.warning("Warning: Cannot determine %s", filename)
        return

    # name = 'vmlinuz-3.2.0-35-generic'
    ver = name.split('-', 1)[1]
    pkgname = '%s-%s' % (KERNEL_PREFIX, ver)
    if pkgname not in cache:
        log.warning("Cannot find kernel package for %s", filename)
        return

    pkg = cache[pkgname]
    return pkg

####

parser = optparse.OptionParser()
parser.add_option('-v', '--verbose', action='store_true', default=False)
parser.add_option('-q', '--quiet', action='store_true', default=False)


def main():
    opts = None
    try:
        opts, args = parser.parse_args()
        if opts.verbose:
            level = logging.DEBUG
        elif opts.quiet:
            level = logging.WARN
        else:
            level = logging.INFO

        logging.basicConfig(level=level, format="%(message)s")

        if args:
            parser.print_help()
            raise Abort("Too many arguments")

        cleaner = Cleaner(opts)
        cleaner.run()

    except Abort as err:
        log.error("Abort: %s", err, exc_info=opts.verbose if opts else True)
        sys.exit(1)

    except KeyboardInterrupt:
        sys.exit(1)


if __name__ == '__main__':
    main()
