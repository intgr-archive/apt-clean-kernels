apt-clean-kernels is a tool for cleaning unnecessary old kernel installs on
Debian-based distros (including Ubuntu). Otherwise, these kernels tend to
accumulate with upgrades and simply waste disk space.

Example:

$ ./apt-clean-kernels.py
Reading package lists... Done
Building dependency tree       
Reading state information... Done
Keeping:
* Installed linux-image-generic-pae: linux-image-2.6.32-45-generic-pae
* Running kernel: linux-image-2.6.32-41-generic-pae
* Symlink /vmlinuz: linux-image-2.6.32-45-generic-pae
* Symlink /vmlinuz.old: linux-image-2.6.32-45-generic
Removing:
* linux-image-2.6.32-41-generic
* linux-image-2.6.32-44-generic-pae
* linux-image-2.6.32-44-generic
* linux-image-2.6.32-43-generic-pae
* linux-image-2.6.32-43-generic
* linux-image-2.6.32-42-generic-pae
* linux-image-2.6.32-42-generic
* linux-image-2.6.32-41-generic
* linux-image-2.6.32-40-generic-pae
* linux-image-2.6.32-40-generic

[sudo] password for user:
Reading package lists... Done
Building dependency tree       
Reading state information... Done
The following packages will be REMOVED:
  linux-image-2.6.32-40-generic linux-image-2.6.32-40-generic-pae
  linux-image-2.6.32-41-generic linux-image-2.6.32-42-generic
  linux-image-2.6.32-42-generic-pae linux-image-2.6.32-43-generic
  linux-image-2.6.32-43-generic-pae linux-image-2.6.32-44-generic
  linux-image-2.6.32-44-generic-pae
0 upgraded, 0 newly installed, 9 to remove and 23 not upgraded.
After this operation, 890MB disk space will be freed.
Do you want to continue [Y/n]? 

